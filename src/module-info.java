module enquete_satisfaction_java {
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires java.sql;
	requires javafx.base;
	requires java.mail;
	
	opens application to javafx.graphics, javafx.fxml;
	opens entites to javafx.base;
}
