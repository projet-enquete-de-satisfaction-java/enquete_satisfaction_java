package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Scanner;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import entites.Apprenant;
import entites.Formation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import services.Dbconfig;
import services.Mail;
import services.ServiceFormulaireAjoutNotation;
import services.ServiceFormulaireEnvoiEmail;

public class ControllerFormulaireEnvoiEmail implements Initializable{
	private Dbconfig mysqlConnect;
	private ServiceFormulaireEnvoiEmail serviceFormulaireEnvoiEmail;
	@FXML 
	private Button btnEnvoiEmail;
	@FXML 
	private Button btnImport;
	@FXML 
	private ComboBox comboTheme;
	@FXML 
	private ComboBox comboFormateur;
	@FXML 
	private ComboBox comboDateFormation;
	@FXML
	private Pane paneEnvoiEmail;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method 
		// Connexion à la bdd et appel du service
		this.mysqlConnect = new Dbconfig();
		this.serviceFormulaireEnvoiEmail = new ServiceFormulaireEnvoiEmail(this.mysqlConnect);

		// Remplir la combo comboFormateur 
		List<String> listeFormateur = this.serviceFormulaireEnvoiEmail.selectFormateurPourEnvoiEmail();
		ObservableList<String> formateur = FXCollections.observableArrayList(listeFormateur);
		this.comboFormateur.setItems(formateur);
	}

	public void allerVueAccueil() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueAccueil.fxml"));
		Scene scene = this.paneEnvoiEmail.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireAjoutNote() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireAjoutNote.fxml"));
		Scene scene = this.paneEnvoiEmail.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireAjoutApprenant() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireAjoutApprenant.fxml"));
		Scene scene = this.paneEnvoiEmail.getScene();	
		scene.setRoot(rootFXML);
	}

	// Remplie la combo comboTheme dès que l'utilisateur à sélectionnée le formateur
	public void remplirTheme() {
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		List<String> listeTheme = this.serviceFormulaireEnvoiEmail.selectThemePourEnvoiEmail(selectedFormateur);
		ObservableList<String> theme = FXCollections.observableArrayList(listeTheme);
		this.comboTheme.setItems(theme);
	}

	// Remplie la combo comboDateFormation dès que l'utilisateur à sélectionnée le theme
	public void remplirDateFormation() {
		String selectedTheme =  (String) this.comboTheme.getValue();
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		List<String> listeDate = this.serviceFormulaireEnvoiEmail.selectDatePourEnvoiEmail(selectedFormateur, selectedTheme);
		ObservableList<String> date_formation = FXCollections.observableArrayList(listeDate);
		this.comboDateFormation.setItems(date_formation);
	}

	// Nettoie le formulaire
	public void clear() {
		this.comboTheme.getItems().clear();
		this.comboFormateur.setValue(null);
		this.comboDateFormation.getItems().clear();
	}

	public void envoyerEmail() {
		String selectedTheme =  (String) this.comboTheme.getValue();
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		String selectedDate =  (String) this.comboDateFormation.getValue();
		List<Apprenant> listeApprenant = this.serviceFormulaireEnvoiEmail.selectApprenantPourEnvoiEmail(selectedFormateur, selectedTheme, selectedDate);

		for(Apprenant apprenant: listeApprenant){
			// Appel de la classe mail
			Mail mail = new Mail();
			try {
				int status = mail.sendMail(apprenant.getEmail(), apprenant.getPrenom()+" "+apprenant.getNom(), selectedTheme, selectedFormateur, selectedDate);
				if (status == 200) {
					System.out.println("Le mail a été envoyé avec succès");
				} else {
					System.out.println("Une erreur est survenue lors de l'envoi du mail");
				}
			} catch (MessagingException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		this.serviceFormulaireEnvoiEmail.updateFormation(selectedFormateur, selectedTheme, selectedDate);
		this.clear();
	}

}
