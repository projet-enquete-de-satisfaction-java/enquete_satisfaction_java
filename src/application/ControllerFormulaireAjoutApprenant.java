package application;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import entites.Apprenant;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import services.Dbconfig;
import services.ServiceFormulaireAjoutApprenant;
import services.ServiceFormulaireAjoutNotation;
import services.ServiceFormulaireEnvoiEmail;

public class ControllerFormulaireAjoutApprenant implements Initializable{
	private Dbconfig mysqlConnect;
	private ServiceFormulaireAjoutApprenant serviceFormulaireAjoutApprenant;
	@FXML 
	private TextField inputNom;
	@FXML 
	private TextField inputPrenom;
	@FXML 
	private TextField inputEmail;
	@FXML 
	private Button btnInsererApprenant;
	@FXML 
	private ComboBox comboTheme;
	@FXML 
	private ComboBox comboFormateur;
	@FXML 
	private ComboBox comboDateFormation;
	@FXML
	private Pane paneAjoutApprenant;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
		// Connection à la BDD et appel du service 
		this.mysqlConnect = new Dbconfig();
		this.serviceFormulaireAjoutApprenant = new ServiceFormulaireAjoutApprenant(this.mysqlConnect);
		
		List<String> listeTheme = this.serviceFormulaireAjoutApprenant.selectTheme();
		ObservableList<String> theme = FXCollections.observableArrayList(listeTheme);
		this.comboTheme.setItems(theme);
	}

	public void allerVueAccueil() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueAccueil.fxml"));
		Scene scene = this.paneAjoutApprenant.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireAjoutNote() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireAjoutNote.fxml"));
		Scene scene = this.paneAjoutApprenant.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireEnvoiEmail() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireEnvoiEmail.fxml"));
		Scene scene = this.paneAjoutApprenant.getScene();	
		scene.setRoot(rootFXML);
	}
	
	// méthode appelée dès qu'on a sélectionné un theme
	public void remplirFormateur() {
		String selectedTheme =  (String) this.comboTheme.getValue();
		List<String> listeFormateur = this.serviceFormulaireAjoutApprenant.selectFormateur(selectedTheme);
		ObservableList<String> formateur = FXCollections.observableArrayList(listeFormateur);
		this.comboFormateur.setItems(formateur);
	}
	
	// méthode appelée dès qu'on a sélectionné un formateur
	public void remplirDateFormation() {
		String selectedTheme =  (String) this.comboTheme.getValue();
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		List<String> listeDate = this.serviceFormulaireAjoutApprenant.selectDate(selectedFormateur, selectedTheme);
		ObservableList<String> date_formation = FXCollections.observableArrayList(listeDate);
		this.comboDateFormation.setItems(date_formation);
	}
	
	// methode appelée pour vider le formulaire après envoi
	public void clear() {
		this.inputNom.setText("");
		this.inputPrenom.setText("");
		this.inputEmail.setText("");
		this.comboTheme.setValue(null);
		this.comboFormateur.getItems().clear();
		this.comboDateFormation.getItems().clear();
	}
	public void ajouterUnApprenant() {
		String nom = this.inputNom.getText();
		String prenom = this.inputPrenom.getText();
		String email = this.inputEmail.getText();
		String themeFormation = (String) this.comboTheme.getValue();
		String formateur = (String) this.comboFormateur.getValue();
		String dateFormation = (String) this.comboDateFormation.getValue();

		this.serviceFormulaireAjoutApprenant.insertApprenant(nom, prenom, email);
		int id_apprenant = this.serviceFormulaireAjoutApprenant.selectApprenantId(nom, prenom);
		int id_formation = this.serviceFormulaireAjoutApprenant.selectFormationId(formateur, themeFormation, dateFormation);
		this.serviceFormulaireAjoutApprenant.insertApprenantFormation(id_apprenant, id_formation);

		this.clear();
	}
}
