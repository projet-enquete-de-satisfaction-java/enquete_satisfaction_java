package application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import entites.Apprenant;
import entites.ListingComplet;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import services.Dbconfig;
import services.ServiceAccueil;
import services.ServiceFormulaireAjoutNotation;

public class ControllerAccueil{
	@FXML 
	private Button btnAjoutNote;
	@FXML
	private Pane paneAccueil;
	@FXML
	private TableView<ListingComplet> tableViewListing;

	@FXML
	private TableColumn<ListingComplet, String> formateurColumn;

	@FXML
	private TableColumn<ListingComplet, String> emailApprenantColumn;

	@FXML
	private TableColumn<ListingComplet, String> themeColumn;

	@FXML
	private TableColumn<ListingComplet, String> dateFormationColumn;

	@FXML
	private TableColumn<ListingComplet, Integer> noteQ1Column;

	@FXML
	private TableColumn<ListingComplet, Integer> noteQ2Column;

	@FXML
	private TableColumn<ListingComplet, Integer> noteQ3Column;

	@FXML
	private TableColumn<ListingComplet, Integer> noteQ4Column;

	@FXML
	private TableColumn<ListingComplet, Integer> totalColumn;

	@FXML
	private Button btnExport;

	@FXML
	private DatePicker date1;

	@FXML
	private DatePicker date2;

	private ServiceAccueil sql;

	@FXML
	public void initialize() {
		//  Création d'un objet config de la base de données
		Dbconfig dbconfig = new Dbconfig();
		// Création d'une instance ServiceAccueil
		this.sql = new ServiceAccueil(dbconfig);
		// Sélection de toutes les formations
		List<ListingComplet> formationList = sql.selectAllFormation();
		// Affectation des données
		tableViewListing.setItems(FXCollections.observableList(formationList));

		// Définition des propriétés de chaque colonne de la table d'affichage
		formateurColumn.setCellValueFactory(new PropertyValueFactory<>("formateur"));
		emailApprenantColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
		themeColumn.setCellValueFactory(new PropertyValueFactory<>("theme"));
		dateFormationColumn.setCellValueFactory(new PropertyValueFactory<>("DateFormation"));
		noteQ1Column.setCellValueFactory(new PropertyValueFactory<>("noteQ1"));
		noteQ2Column.setCellValueFactory(new PropertyValueFactory<>("noteQ2"));
		noteQ3Column.setCellValueFactory(new PropertyValueFactory<>("noteQ3"));
		noteQ4Column.setCellValueFactory(new PropertyValueFactory<>("noteQ4"));
		totalColumn.setCellValueFactory(new PropertyValueFactory<>("total"));
	}


	public void exportData() {
		// Création d'un objet config de la base de données
		Dbconfig dbconfig = new Dbconfig();
		Connection connection = dbconfig.getDbconfig();

		// Récupération valeurs date1 & date2
		LocalDate date1Value = date1.getValue();
		LocalDate date2Value = date2.getValue();
		String query = "SELECT formation.formateur,AVG(enquete_satisfaction.noteQ1), AVG(enquete_satisfaction.noteQ2), AVG(enquete_satisfaction.noteQ3), AVG(enquete_satisfaction.noteQ4), SUM(enquete_satisfaction.noteQ1 + enquete_satisfaction.noteQ2 + enquete_satisfaction.noteQ3 + enquete_satisfaction.noteQ4) as total_notes FROM enquete_satisfaction JOIN formation ON enquete_satisfaction.id_formation = formation.id WHERE enquete_satisfaction.date_envoi BETWEEN '" + date1Value.toString() + "' AND '" + date2Value.toString() + "' GROUP BY formation.formateur;";
		//Création d'un objet pour choisir ou sauvegarder le fichier
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Enregistrer le fichier exporté");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().add(
				new FileChooser.ExtensionFilter("CSV Files", "*.csv")
				);
		//Affiche la fenêtre de sélection et récupère le fichier sélectionné
		File file = fileChooser.showSaveDialog(this.btnExport.getScene().getWindow());
		if (file != null) {
			try (FileWriter writer = new FileWriter(file)) {
				// Ecrire les en-têtes
				writer.append("Formateur,Moyenne Q1,Moyenne Q2,Moyenne Q3,Moyenne Q4,Total");
				writer.append(System.lineSeparator());
				// Execute la query
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(query);

				// Parcours résultats de la query  et écriture des données
				while (rs.next()) {
					writer.append(rs.getString("formateur"));
					writer.append(",");
					writer.append(rs.getString("AVG(enquete_satisfaction.noteQ1)"));
					writer.append(",");
					writer.append(rs.getString("AVG(enquete_satisfaction.noteQ2)"));
					writer.append(",");
					writer.append(rs.getString("AVG(enquete_satisfaction.noteQ3)"));
					writer.append(",");
					writer.append(rs.getString("AVG(enquete_satisfaction.noteQ4)"));
					writer.append(",");
					writer.append(rs.getString("total_notes"));
					writer.append(System.lineSeparator());
				}
				writer.flush();
				writer.close();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Exportation réussie");
				alert.setHeaderText(null);
				alert.setContentText("Les données ont été exportées avec succès!");
				alert.showAndWait();
				rs.close();
			} catch (SQLException | IOException e) {

				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Erreur d'exportation");
				alert.setHeaderText(null);
				alert.setContentText("Une erreur est survenue lors de l'exportation des données: " + e.getMessage());
				alert.showAndWait();
			}
		}
	}

	public void allerFormulaireAjoutNote() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireAjoutNote.fxml"));
		Scene scene = this.paneAccueil.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireAjoutApprenant() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireAjoutApprenant.fxml"));
		Scene scene = this.paneAccueil.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireEnvoiEmail() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireEnvoiEmail.fxml"));
		Scene scene = this.paneAccueil.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerVueGraph() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueGraph.fxml"));
		Scene scene = this.paneAccueil.getScene();    
		scene.setRoot(rootFXML);
	}
}
