package application;

import java.io.IOException;
import java.util.List;

import entites.Formation;
import entites.ListingComplet;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;
import services.Dbconfig;
import services.ServiceAccueil;


public class ControllerGraphique {
	@FXML
	private Pane paneGraph;
	@FXML
	private BarChart<String, Number> barChart;
	private Dbconfig dbconfig = new Dbconfig();
	private ServiceAccueil sql = new ServiceAccueil(dbconfig);
	private List<ListingComplet> formationList = sql.selectAllFormation();

	@FXML
	public void initialize() {
		// Creation des objets pour les axes X et Y
		CategoryAxis xAxis = new CategoryAxis();
		xAxis.setLabel("Formateurs");
		NumberAxis yAxis = new NumberAxis();
		yAxis.setLabel("Total");
		// Suppression des données précédemment chargées
		barChart.getData().clear();
		// Creation d'une série de données pour le graphique à barres
		XYChart.Series<String, Number> series = new XYChart.Series<>();
		// Boucle pour ajouter les données à la série
		for (ListingComplet formation : formationList) {
			series.getData().add(new XYChart.Data<>(formation.getFormateur(), formation.getTotal()));
		}
		// Ajout de la série de données au graphique à barres
		barChart.getData().add(series);
	}

	public void allerVueAccueil() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueAccueil.fxml"));
		Scene scene = this.paneGraph.getScene();    
		scene.setRoot(rootFXML);
	}
}
