package application;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import entites.Apprenant;
import entites.EnqueteSatisfaction;
import entites.Formation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import services.Dbconfig;
import services.ServiceFormulaireAjoutNotation;

public class ControllerFormulaireAjoutNote implements Initializable{
	private Dbconfig mysqlConnect;
	private ServiceFormulaireAjoutNotation serviceFormulaireAjoutNotation;
	@FXML 
	private Button btnEnvoiEmail;
	@FXML 
	private Button btnAjoutNote;
	@FXML 
	private Button bntAjoutApprenant;
	@FXML 
	private Button btnEnregistrementNotation;
	@FXML 
	private ComboBox comboApprenant;
	@FXML 
	private ComboBox comboFormateur;
	@FXML 
	private ComboBox comboDateFormation;
	@FXML 
	private ComboBox comboTheme;
	@FXML 
	private TextField inputNoteQ1;
	@FXML 
	private TextField inputNoteQ2;
	@FXML 
	private TextField inputNoteQ3;
	@FXML 
	private TextField inputNoteQ4;
	@FXML
	private Pane paneAjoutNote;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		//Connexion à la bdd et appel du service
		this.mysqlConnect = new Dbconfig();
		this.serviceFormulaireAjoutNotation = new ServiceFormulaireAjoutNotation(this.mysqlConnect);

		// Remplissage comboApprenant à l'initialisaton
		List<Apprenant> listeApprenant = this.serviceFormulaireAjoutNotation.selectApprenants();
		ObservableList<String> nomApprenant = FXCollections.observableArrayList();
		for(Apprenant apprenant: listeApprenant){
			nomApprenant.add(apprenant.getPrenom()+ " "+apprenant.getNom());
		}
		this.comboApprenant.setItems((ObservableList) nomApprenant);
	}

	public void allerVueAccueil() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueAccueil.fxml"));
		Scene scene = this.paneAjoutNote.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireAjoutApprenant() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireAjoutApprenant.fxml"));
		Scene scene = this.paneAjoutNote.getScene();	
		scene.setRoot(rootFXML);
	}

	public void allerFormulaireEnvoiEmail() throws IOException {
		Parent rootFXML = FXMLLoader.load(getClass().getResource("VueFormulaireEnvoiEmail.fxml"));
		Scene scene = this.paneAjoutNote.getScene();	
		scene.setRoot(rootFXML);
	}
	
	// méthode appelé dès qu'on a sélectionné un apprenant
	public void remplirFormateur() {
		String selectedApprenant =  (String) this.comboApprenant.getValue();
		List<String> listeFormation = this.serviceFormulaireAjoutNotation.selectFormateur(selectedApprenant);
		ObservableList<String> formateur = FXCollections.observableArrayList(listeFormation);
		this.comboFormateur.setItems(formateur);
	}
	
    // méthode appelé dès qu'on a sélectionné un formateur
	public void remplirTheme() {
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		String selectedApprenant =  (String) this.comboApprenant.getValue();
		List<String> listeTheme = this.serviceFormulaireAjoutNotation.selectTheme(selectedApprenant, selectedFormateur);
		ObservableList<String> theme = FXCollections.observableArrayList(listeTheme);
		this.comboTheme.setItems(theme);
	}
	
	// méthode appelé dès qu'on a sélectionné un theme
	public void remplirDateFormation() {
		String selectedTheme =  (String) this.comboTheme.getValue();
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		String selectedApprenant =  (String) this.comboApprenant.getValue();
		List<String> listeDate = this.serviceFormulaireAjoutNotation.selectDateFormation(selectedApprenant, selectedFormateur, selectedTheme);
		ObservableList<String> date_formation = FXCollections.observableArrayList(listeDate);
		this.comboDateFormation.setItems(date_formation);
	}
	
	// méthode appelé à l'envoi du formaulaire pour vider le formulaire
	public void clear() {
		this.inputNoteQ1.setText("");
		this.inputNoteQ2.setText("");
		this.inputNoteQ3.setText("");
		this.inputNoteQ4.setText("");
		this.comboApprenant.setValue(null);
		this.comboDateFormation.getItems().clear();
		this.comboTheme.getItems().clear();
		this.comboFormateur.getItems().clear();
	}
	
	public void enregistrerUneNotation() {
		String selectedTheme =  (String) this.comboTheme.getValue();
		String selectedFormateur =  (String) this.comboFormateur.getValue();
		String selectedApprenant =  (String) this.comboApprenant.getValue();
		String selectedDateFormation =  (String) this.comboDateFormation.getValue();
		Float noteQ1 = Float.parseFloat(this.inputNoteQ1.getText());
		Float noteQ2 = Float.parseFloat(this.inputNoteQ2.getText());
		Float noteQ3 = Float.parseFloat(this.inputNoteQ3.getText());
		Float noteQ4 = Float.parseFloat(this.inputNoteQ4.getText());
		ZoneId zonedId = ZoneId.of("Europe/Paris");
		LocalDate today = LocalDate.now( zonedId );

		int apprenant_id = this.serviceFormulaireAjoutNotation.selectApprenantId(selectedApprenant);
		int formation_id = this.serviceFormulaireAjoutNotation.selectFormationId(selectedFormateur, selectedTheme, selectedDateFormation);

		this.serviceFormulaireAjoutNotation.insertNotation(apprenant_id, formation_id, noteQ1, noteQ2, noteQ3, noteQ4, today);

		this.clear();
	}


}

