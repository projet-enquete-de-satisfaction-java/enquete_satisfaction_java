package entites;

public class EnqueteSatisfaction {
	private int id;
	// Réfléchir en poo et donc ne pas mettre les id mais un objet apprenant et un objet formation
	private int id_apprenant;
	private int id_formation;
	private float noteQ1;
	private float noteQ2;
	private float noteQ3;
	private float noteQ4;
	private String date_formation;

	// Constructeur
	public EnqueteSatisfaction(int id, int id_apprenant, int id_formation, float noteQ1, float noteQ2, float noteQ3,
			float noteQ4, String date_formation) {
		this.id = id;
		this.id_apprenant = id_apprenant;
		this.id_formation = id_formation;
		this.noteQ1 = noteQ1;
		this.noteQ2 = noteQ2;
		this.noteQ3 = noteQ3;
		this.noteQ4 = noteQ4;
		this.date_formation = date_formation;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_apprenant() {
		return id_apprenant;
	}
	public void setId_apprenant(int id_apprenant) {
		this.id_apprenant = id_apprenant;
	}
	public int getId_formation() {
		return id_formation;
	}
	public void setId_formation(int id_formation) {
		this.id_formation = id_formation;
	}
	public float getNoteQ1() {
		return noteQ1;
	}
	public void setNoteQ1(float noteQ1) {
		this.noteQ1 = noteQ1;
	}
	public float getNoteQ2() {
		return noteQ2;
	}
	public void setNoteQ2(float noteQ2) {
		this.noteQ2 = noteQ2;
	}
	public float getNoteQ3() {
		return noteQ3;
	}
	public void setNoteQ3(float noteQ3) {
		this.noteQ3 = noteQ3;
	}
	public float getNoteQ4() {
		return noteQ4;
	}
	public void setNoteQ4(float noteQ4) {
		this.noteQ4 = noteQ4;
	}
	public String getDate_formation() {
		return date_formation;
	}
	public void setDate_formation(String date_formation) {
		this.date_formation = date_formation;
	}
}
