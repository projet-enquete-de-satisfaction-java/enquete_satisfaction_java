package entites;

public class ListingComplet {

		private int id;
		private String formateur;
		private String date_formation;
		private String theme;
		private String nom;
		private String prenom;
		private String email;
		private int noteQ1;
		private int noteQ2;
		private int noteQ3;
		private int noteQ4;
		private int total;



	    public ListingComplet(int id, String formateur,String date_formation,String theme, String nom, String prenom, String email, int noteQ1, int noteQ2, int noteQ3, int noteQ4, int total) {
	        this.id = id;
	        this.formateur = formateur;
	        this.date_formation = date_formation;
	        this.theme = theme;
	        this.nom = nom;
	        this.prenom = prenom;
	        this.email = email;
	        this.noteQ1 = noteQ1;
	        this.noteQ2 = noteQ2;
	        this.noteQ3 = noteQ3;
	        this.noteQ4 = noteQ4;
	        this.total = total;
	    }

	    public int getId() {
	        return id;
	    }

	    public String getFormateur() {
	        return formateur;
	    }

	    public String getDateFormation() {
	        return date_formation;
	    }

	    public String getTheme() {
	        return theme;
	    }

	    public String getNom() {
	        return nom;
	    }

	    public String getPrenom() {
	        return prenom;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public int getNoteQ1() {
	        return noteQ1;
	    }

	    public int getNoteQ2() {
	        return noteQ2;
	    }

	    public int getNoteQ3() {
	        return noteQ3;
	    }

	    public int getNoteQ4() {
	        return noteQ4;
	    }
	    
	    public int getTotal() {
	        return total;
	    }
	}


