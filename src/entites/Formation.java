package entites;

public class Formation {
	private int id;
	private String formateur;
	private String date_formation;
	private String theme;
	private int email_envoye;
	
	public Formation(int id, String formateur, String date_formation, String theme, int email_envoye) {
		this.id = id;
		this.formateur = formateur;
		this.date_formation = date_formation;
		this.theme = theme;
		this.setEmail_envoye(email_envoye);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFormateur() {
		return formateur;
	}

	public void setFormateur(String formateur) {
		this.formateur = formateur;
	}

	public String getDate_formation() {
		return date_formation;
	}

	public void setDate_formation(String date_formation) {
		this.date_formation = date_formation;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public int getEmail_envoye() {
		return email_envoye;
	}

	public void setEmail_envoye(int email_envoye) {
		this.email_envoye = email_envoye;
	}
}
