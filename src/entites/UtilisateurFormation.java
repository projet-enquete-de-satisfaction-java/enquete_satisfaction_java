package entites;

public class UtilisateurFormation {
	private int id_apprenant;
	private int id_formation;

	public UtilisateurFormation(int id_apprenant, int id_formation) {
		this.id_apprenant = id_apprenant;
		this.id_formation = id_formation;
	}

	public int getId_apprenant() {
		return id_apprenant;
	}
	public void setId_apprenant(int id_apprenant) {
		this.id_apprenant = id_apprenant;
	}
	public int getId_formation() {
		return id_formation;
	}
	public void setId_formation(int id_formation) {
		this.id_formation = id_formation;
	}
}
