package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entites.Apprenant;
import entites.Formation;

public class ServiceFormulaireEnvoiEmail {
	private Connection connection;

	public ServiceFormulaireEnvoiEmail(Dbconfig dbconfig) {
		this.connection = dbconfig.getDbconfig();
	}

	// Sélection du formateur pour remplir la combo du formateur du formulaire d'envoi d'emails
	public List<String> selectFormateurPourEnvoiEmail() {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeformateur = new ArrayList<String>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT DISTINCT formateur FROM formation"
					+ " where email_envoye = 0 and date_formation < CURDATE();");

			// Boucle sur les résultats pour insérer dans la liste listeFormateur
			while (resultat.next()) {
				String formateur = resultat.getString("formateur");
				listeformateur.add(formateur);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeformateur;
	}

	// Sélection du theme pour remplir la combo theme du formulaire d'envoi d'emails en fonction du formateur choisi
	public List<String> selectThemePourEnvoiEmail(String formateur) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeTheme = new ArrayList<String>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT DISTINCT theme FROM formation "
					+ "where email_envoye = 0 and formateur = '"+formateur+"' and date_formation < CURDATE();");

			// Boucle sur les résultats pour insérer dans la liste listeTheme
			while (resultat.next()) {
				String theme = resultat.getString("theme");
				listeTheme.add(theme);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeTheme;
	}
	
	// Sélection du la date pour remplir la combo date formation du formulaire d'envoi d'emails en fonction du formateuret du theme choisi 
	public List<String> selectDatePourEnvoiEmail(String formateur, String theme) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeDateFormation = new ArrayList<String>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT date_formation FROM formation "
					+ "where email_envoye = 0 and formateur = '"+formateur+"' and theme = '"+theme+"' and date_formation < CURDATE();");

			// Boucle sur les résultats pour insérer dans la liste listeDateFormation
			while (resultat.next()) {
				String date_formation = resultat.getString("date_formation");
				listeDateFormation.add(date_formation);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeDateFormation;
	}

	// Sélection des appreant à qui envoyer l'email en fonction des choix efféctué dans les combo
	public List<Apprenant> selectApprenantPourEnvoiEmail(String formateur, String theme, String date_formation){
		Statement statement = null;
		ResultSet resultat = null;
		List<Apprenant> listeApprenant = new ArrayList<Apprenant>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT a.id, a.nom, a.prenom, a.email\r\n"
					+ "FROM apprenant a\r\n"
					+ "JOIN utilisateur_formation uf ON a.id = uf.id_apprenant\r\n"
					+ "JOIN formation f ON uf.id_formation = f.id\r\n"
					+ "WHERE f.formateur = '"+formateur+"' AND f.theme = '"+theme+"' AND f.date_formation = '"+date_formation+"';");

			// Boucle sur les résultats pour insérer dans la liste listeApprenant
			while (resultat.next()) {
				int id = resultat.getInt("id");
				String nom = resultat.getString("nom");
				String prenom = resultat.getString("prenom");
				String email = resultat.getString("email");
				Apprenant instanceApprenant = new Apprenant(id, nom, prenom, email);
				listeApprenant.add(instanceApprenant);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeApprenant;
	}

	// Mise à jour de la colonne email_envoye, passer de 0 à 1 pour signifier que pour cette formation on a bien envoyé les emails
	public void updateFormation(String formateur, String theme, String date_formation) {
		Statement statement = null;
		int email_envoye = 1;
		try {
			statement = this.connection.createStatement();
			String sql = "update formation set email_envoye = ? where formateur = '"+formateur+"' "
					+ "and theme = '"+theme+"' and date_formation = '"+date_formation+"';";

			PreparedStatement preparedStmt = this.connection.prepareStatement(sql);
			preparedStmt.setInt(1, email_envoye);

			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'update de la colonne email_envoye");
			e.printStackTrace();
		}
	}
}
