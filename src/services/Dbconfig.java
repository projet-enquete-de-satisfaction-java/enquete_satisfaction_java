package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Dbconfig {

	private Connection connection;

	public Dbconfig() {
		// Chargement du driver JDBC MySQL
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Erreur lors du chargement du driver JDBC MySQL");
			e.printStackTrace();
			return;
		}

		// Connexion à la base de données
		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/projet_enquete",
					"cha", "123");
		} catch (SQLException e) {
			System.out.println("Erreur lors de la connexion à la base de données");
			e.printStackTrace();
			return;
		}
	}

	public Connection getDbconfig() {
		return connection;
	}

}




