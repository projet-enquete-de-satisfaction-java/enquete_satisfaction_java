package services;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mail {
	private static final String FROM = "nicolas.gilles01@gmail.com";
	private static final String FROMNAME = "E-FormationPro";
	private static final String SMTP_USERNAME = "nicolas.gilles01@gmail.com";
	private static final String SMTP_PASSWORD = "dzrvpvphllxkdpyt";
	private static final String CONFIGSET = "ConfigSet";
	private static final String HOST = "smtp.gmail.com";
	private static final int PORT = 587;

	//Permet de s'authentifier auprès du serveur SMTP
	private static Authenticator authenticator = new Authenticator() {
		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(SMTP_USERNAME, SMTP_PASSWORD);
		}
	};

	public Mail() {}

	public int sendMail(String to, String apprenant, String theme, String formateur, String date_formation) throws MessagingException, UnsupportedEncodingException, AddressException {
		String object = "E-FormationPro satisfaction";
		String body = String.join(
				System.getProperty("line.separator"),
				"<!DOCTYPE html>\r\n"
						+ "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\r\n"
						+ "<head>\r\n"
						+ "  <meta charset=\"UTF-8\">\r\n"
						+ "  <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\">\r\n"
						+ "  <meta name=\"x-apple-disable-message-reformatting\">\r\n"
						+ "  <title></title>\r\n"
						+ "  <!--[if mso]>\r\n"
						+ "  <noscript>\r\n"
						+ "    <xml>\r\n"
						+ "      <o:OfficeDocumentSettings>\r\n"
						+ "        <o:PixelsPerInch>96</o:PixelsPerInch>\r\n"
						+ "      </o:OfficeDocumentSettings>\r\n"
						+ "    </xml>\r\n"
						+ "  </noscript>\r\n"
						+ "  <![endif]-->\r\n"
						+ "  <style>\r\n"
						+ "    table, td, div, h1, p {font-family: Arial, sans-serif;}\r\n"
						+ "  </style>\r\n"
						+ "</head>\r\n"
						+ "<body style=\"margin:0;padding:0;\">\r\n"
						+ "  <table role=\"presentation\" style=\"width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;\">\r\n"
						+ "    <tr>\r\n"
						+ "      <td align=\"center\" style=\"padding:0;\">\r\n"
						+ "        <table role=\"presentation\" style=\"width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;\">\r\n"
						+ "          <tr>\r\n"
						+ "            <td align=\"center\" style=\"padding:40px 0 30px 0;background:#70bbd9;\">\r\n"
						+ "              <img src=\"https://www.pngplay.com/wp-content/uploads/6/Email-Logo-Transparent-Background.png\" alt=\"\" width=\"300\" style=\"height:auto;display:block;\" />\r\n"
						+ "            </td>\r\n"
						+ "          </tr>\r\n"
						+ "          <tr>\r\n"
						+ "            <td style=\"padding:36px 30px 42px 30px;\">\r\n"
						+ "              <table role=\"presentation\" style=\"width:100%;border-collapse:collapse;border:0;border-spacing:0;\">\r\n"
						+ "                <tr>\r\n"
						+ "                  <td style=\"padding:0 0 36px 0;color:#153643;\">\r\n"
						+ "                    <h1 style=\"font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;\">Merci de votre confiance !</h1>\r\n"
						+ "                    <p style=\"margin:0 0 12px 0;font-size:16px;line-height:24px;font-family:Arial,sans-serif;\">"
						+ "Bonjour "+apprenant+",<br><br>"
						+ "Nous espérons que votre formation de "+theme+" du "+date_formation+",<br>"
						+ "avec "+formateur+" s'est bien déroulé.<br><br>"
						+ "Nous aimerions savoir "
						+ "comment vous vous sentez au sujet de celle-ci et nous vous contacterons prochainement "
						+ "pour une enquête de satisfaction par téléphone.<br>"
						+ "Cette enquête nous permettra de mieux comprendre "
						+ "votre expérience et de nous assurer que nous fournissons la meilleure qualité de formation possible.<br>"
						+ "Elle ne prendra que quelques minutes de votre temps et vos commentaires seront "
						+ "très appréciés.<br>"
						+ "Nous vous contacterons dans les prochains jours pour fixer un rendez-vous pour l'enquête.<br><br>"
						+ "Si vous avez des questions ou des préoccupations, "
						+ "n'hésitez pas à nous contacter.<br><br>"
						+ "Nous vous remercions d'avance pour votre participation.<br><br>"
						+ "Cordialement,<br><br>"
						+ "L'équipe E-FormationPro."
						+ "                     </p>"
						+ "					 </td>\r\n"
						+ "				   </tr>\r\n"
						+ "				 </table>\r\n"
						+ "				</td>\r\n"
						+ "			 </tr>\r\n"
						+ "			</tabl>\r\n"
						+ "		   </td>\r\n"
						+ "		  </tr>\r\n"
						+ "		</tabl>\r\n"
						+ "	</body>\r\n"
						+ "</html>"
				);
		Properties props = System.getProperties();
		// Set protocol smtp
		props.put("mail.transport.protocol", "smtp");
		// Set port
		props.put("mail.smtp.port", PORT);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		// Ajout du serveur SMTP au "ssl trust" pour éviter les problèmes de sécurité
		props.put("mail.smtp.ssl.trust", HOST);
		Session session = Session.getInstance(props, authenticator);
		MimeMessage msg = new MimeMessage(session);
		// Set expéditeur
		msg.setFrom(new InternetAddress(FROM,FROMNAME));
		// Set destinataire
		msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
		// Set objet
		msg.setSubject(object);
		// Ajout du contenu
		msg.setContent(body,"text/html");
		msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
		// Connexion au serveur SMTP
		Transport transport = session.getTransport();
		transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();
		return 200;
	}
}
