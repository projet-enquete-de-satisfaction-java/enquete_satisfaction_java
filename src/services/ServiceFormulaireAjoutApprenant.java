package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ServiceFormulaireAjoutApprenant {
	private Connection connection;

	// Appel de Dbconfig pour se connecter à notre bdd 
	public ServiceFormulaireAjoutApprenant(Dbconfig dbconfig) {
		this.connection = dbconfig.getDbconfig();
	}

	// Selection des thèmes pour remplir la combo du formulaire ajout apprenant
	public List<String> selectTheme() {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeTheme = new ArrayList<String>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT DISTINCT theme FROM formation where date_formation > CURDATE();");

			// Boucle sur les résultat présent dans la table pour les mettre dans une liste
			while (resultat.next()) {
				String theme = resultat.getString("theme");
				listeTheme.add(theme);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de la requête selectTheme pour le formulaire ajout apprenant");
			e.printStackTrace();
		} 
		return listeTheme;
	}

	// Selection des formateurs pour remplir la combo du formulaire ajout apprenant
	public List<String> selectFormateur(String theme) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeformateur = new ArrayList<String>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT DISTINCT formateur FROM formation where theme = '"+theme+"' and date_formation > CURDATE();");

			// Boucle sur les résultat présent dans la table pour les mettre dans une liste
			while (resultat.next()) {
				String formateur = resultat.getString("formateur");
				listeformateur.add(formateur);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de la requête selectFormateur pour le formulaire ajout apprenant");
			e.printStackTrace();
		} 
		return listeformateur;
	}

	// Selection des dates pour remplir la combo du formulaire ajout apprenant
	public List<String> selectDate(String formateur, String theme) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeDateFormation = new ArrayList<String>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT date_formation FROM formation "
					+ "where formateur = '"+formateur+"' and theme = '"+theme+"' and date_formation > CURDATE();");

			// Boucle sur les résultat présent dans la table pour les mettre dans une liste
			while (resultat.next()) {
				String date_formation = resultat.getString("date_formation");
				listeDateFormation.add(date_formation);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de la requête selectDate pour le formulaire ajout apprenant");
			e.printStackTrace();
		} 
		return listeDateFormation;
	}
	
	// Selection de l'id de l'apprenant pour l'insertion dans la table utilisateur_formation
	public int selectApprenantId(String nom, String prenom) {
		Statement statement = null;
		ResultSet resultat = null;
		int id = 0;

		try {
			statement = this.connection.createStatement();
			String sql = "SELECT id "
					+ "FROM apprenant "
					+ "WHERE nom = '"+nom+"' and prenom = '"+prenom+"';";
			resultat = statement.executeQuery(sql);
			if (resultat.next()) {
				id = resultat.getInt("id");
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de la requête selectApprenantId");
			e.printStackTrace();
		} 
		return id;
	}
	
	// Selection de l'id de la formation pour l'insertion dans la table utilisateur_formation
	public int selectFormationId(String formateur, String theme, String date_formation) {
		Statement statement = null;
		ResultSet resultat = null;
		int id = 0;
		try {
			statement = this.connection.createStatement();
			String sql = "SELECT id "
					+ "FROM formation "
					+ "WHERE formateur = '"+formateur+"' and theme = '"+theme+"' and date_formation = '"+date_formation+"';";
			resultat = statement.executeQuery(sql);
			if (resultat.next()) {
				id = resultat.getInt("id");
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de la requête selectFormationId");
			e.printStackTrace();
		} 
		return id;
	}

	// insertion d'un apprenant dans la table Apprenant
	public void insertApprenant(String nom, String prenom, String email) {
		Statement statement = null;
		try {
			statement = this.connection.createStatement();
			String sql = "insert into apprenant (nom, prenom, email) "
					+ "values (?,?,?);";

			PreparedStatement preparedStmt = this.connection.prepareStatement(sql);
			preparedStmt.setString(1, nom);
			preparedStmt.setString(2, prenom);
			preparedStmt.setString(3, email);

			preparedStmt.execute();
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'insertion d'un apprenant");
			e.printStackTrace();
		}
	}

	// insertion de l'id de l'apprenant et de l'id de la formation dans utilisateur formation pour attribuer une formation à un apprenant
	public void insertApprenantFormation(int id_apprenant, int id_formation) {
		Statement statement = null;
		try {
			statement = this.connection.createStatement();
			String sql = "insert into utilisateur_formation (id_apprenant, id_formation) values (?,?);";

			PreparedStatement preparedStmt = this.connection.prepareStatement(sql);
			preparedStmt.setInt(1, id_apprenant);
			preparedStmt.setInt(2, id_formation);

			preparedStmt.execute();
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'insertion dans la table utilisateur_formation");
			e.printStackTrace();
		}
	}
}
