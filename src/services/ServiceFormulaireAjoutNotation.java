package services;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import entites.Apprenant;
import entites.EnqueteSatisfaction;
import entites.Formation;

public class ServiceFormulaireAjoutNotation {
	private Connection connection;

	public ServiceFormulaireAjoutNotation(Dbconfig dbconfig) {
		this.connection = dbconfig.getDbconfig();
	}

	// Requête select pour la table apprenant pour remplir la combo apprenant du formulaire d'ajout de notes
	public List<Apprenant> selectApprenants() {
		Statement statement = null;
		ResultSet resultat = null;
		List<Apprenant> listeApprenant = new ArrayList<Apprenant>();

		try {
			statement = this.connection.createStatement();
			resultat = statement.executeQuery("SELECT * FROM apprenant");

			// Boucle sur les résultats et insertion des résultats dans la liste
			while (resultat.next()) {
				int id_apprenant = resultat.getInt("id");
				String prenom = resultat.getString("prenom");
				String nom = resultat.getString("nom");
				String email = resultat.getString("email");
				Apprenant apprenantFormation = new Apprenant(id_apprenant, prenom, nom, email);
				listeApprenant.add(apprenantFormation);
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeApprenant;
	}

	// Requête select pour le formateur pour remplir la combo formateur en fonction de l'apprenant
	public List<String> selectFormateur(String apprenant) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeFormation = new ArrayList<String>();

		// Split de l'apprenant, pour séparer le prenom du nom
		String[] parts = apprenant.split(" ");
		String nom = parts[0]; 
		String prenom = parts[1]; 

		try {
			statement = this.connection.createStatement();
			String sql = "SELECT DISTINCT formateur "
					+ "FROM formation "
					+ "JOIN utilisateur_formation ON formation.id = utilisateur_formation.id_formation "
					+ "JOIN apprenant ON utilisateur_formation.id_apprenant = apprenant.id "
					+ "WHERE formation.email_envoye = 1 and apprenant.nom = '"+nom+"' AND apprenant.prenom = '"+prenom+"' AND"
					+ " NOT EXISTS (SELECT * FROM enquete_satisfaction "
					+ "WHERE utilisateur_formation.id_formation = enquete_satisfaction.id_formation "
					+ "AND utilisateur_formation.id_apprenant = enquete_satisfaction.id_apprenant);";
			resultat = statement.executeQuery(sql);

			// Boucle sur les résultats et insertion des résultats dans la liste
			while (resultat.next()) {
				String formateur = resultat.getString("formateur");
				listeFormation.add(formateur);

			}

		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeFormation;
	}

	// Requête select pour le theme pour remplir la combo theme en fonction de l'apprenant et du formateur choisis
	public List<String> selectTheme(String apprenant, String formateur) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeTheme = new ArrayList<String>();

		// Split de l'apprenant, pour séparer le prenom du nom
		String[] parts = apprenant.split(" ");
		String nom = parts[0]; 
		String prenom = parts[1]; 

		try {
			statement = this.connection.createStatement();
			String sql = "SELECT DISTINCT theme "
					+ "FROM formation "
					+ "JOIN utilisateur_formation ON formation.id = utilisateur_formation.id_formation "
					+ "JOIN apprenant ON utilisateur_formation.id_apprenant = apprenant.id "
					+ "WHERE formation.email_envoye = 1 and apprenant.nom = '"+nom+"' AND apprenant.prenom = '"+prenom+"' AND formation.formateur = '"+formateur+"'AND NOT EXISTS (SELECT * FROM enquete_satisfaction "
					+ "WHERE utilisateur_formation.id_formation = enquete_satisfaction.id_formation "
					+ "AND utilisateur_formation.id_apprenant = enquete_satisfaction.id_apprenant);";
			resultat = statement.executeQuery(sql);

			// Boucle sur les résultats et insertion des résultats dans la liste
			while (resultat.next()) {
				String theme = resultat.getString("theme");
				listeTheme.add(theme);

			}

		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeTheme;
	}

	// Requête select pour la date de formation pour remplir la combo date de formation en fonction de l'apprenant, du formateur choisis et du thème
	public List<String> selectDateFormation(String apprenant, String formateur, String theme) {
		Statement statement = null;
		ResultSet resultat = null;
		List<String> listeDateFormation = new ArrayList<String>();

		// Split de l'apprenant, pour séparer le prenom du nom
		String[] parts = apprenant.split(" ");
		String nom = parts[0]; 
		String prenom = parts[1]; 

		try {
			statement = this.connection.createStatement();
			String sql = "SELECT DISTINCT date_formation "
					+ "FROM formation "
					+ "JOIN utilisateur_formation ON formation.id = utilisateur_formation.id_formation "
					+ "JOIN apprenant ON utilisateur_formation.id_apprenant = apprenant.id "
					+ "WHERE formation.email_envoye = 1 and apprenant.nom = '"+nom+"' AND apprenant.prenom = '"+prenom+"' AND "
					+ "formation.formateur = '"+formateur+"' AND formation.theme = '"+theme+"' "
					+ "AND NOT EXISTS (SELECT * FROM enquete_satisfaction "
					+ "WHERE utilisateur_formation.id_formation = enquete_satisfaction.id_formation "
					+ "AND utilisateur_formation.id_apprenant = enquete_satisfaction.id_apprenant);";
			resultat = statement.executeQuery(sql);

			// Boucle sur les résultats et insertion des résultats dans la liste
			while (resultat.next()) {
				String date_formation = resultat.getString("date_formation");
				listeDateFormation.add(date_formation);

			}

		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return listeDateFormation;
	}
	
	// Selet de l'id de l'apprenant pour l'insertion dans la table enquete_satisfaction
	public int selectApprenantId(String apprenant) {
		Statement statement = null;
		ResultSet resultat = null;
		int id = 0;

		// Split de l'apprenant, pour séparer le prenom du nom
		String[] parts = apprenant.split(" ");
		String nom = parts[0]; 
		String prenom = parts[1];

		try {
			statement = this.connection.createStatement();
			String sql = "SELECT id "
					+ "FROM apprenant "
					+ "WHERE nom = '"+nom+"' and prenom = '"+prenom+"';";
			resultat = statement.executeQuery(sql);
			if (resultat.next()) {
				id = resultat.getInt("id");
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return id;
	}

	// Selet de l'id de la formation pour l'insertion dans la table enquete_satisfaction
	public int selectFormationId(String formateur, String theme, String date_formation) {
		Statement statement = null;
		ResultSet resultat = null;
		int id = 0;
		try {
			statement = this.connection.createStatement();
			String sql = "SELECT id "
					+ "FROM formation "
					+ "WHERE formateur = '"+formateur+"' and theme = '"+theme+"' and date_formation = '"+date_formation+"';";
			resultat = statement.executeQuery(sql);
			if (resultat.next()) {
				id = resultat.getInt("id");
			}
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
		} 
		return id;
	}
	
	// Insertion d'une notation dans la table enquete_satisfaction
	public void insertNotation(int apprenant_id, int formation_id, float noteQ1, float noteQ2, float noteQ3, float noteQ4, LocalDate today) {
		Statement statement = null;
		java.sql.Date todaySqlDate = java.sql.Date.valueOf(today);
		try {
			statement = this.connection.createStatement();
			String sql = "insert into enquete_satisfaction (id_formation, id_apprenant, noteQ1, noteQ2, noteQ3, noteQ4, date_envoi)"
					+ "values (?,?,?,?,?,?,?);";
			
			// requête préparé pour éviter les injections SQL
			PreparedStatement preparedStmt = this.connection.prepareStatement(sql);
			preparedStmt.setInt(1, formation_id);
			preparedStmt.setInt(2, apprenant_id);
			preparedStmt.setFloat(3, noteQ1);
			preparedStmt.setFloat(4, noteQ2);
			preparedStmt.setFloat(5, noteQ3);
			preparedStmt.setFloat(6, noteQ4);
			preparedStmt.setDate(7, todaySqlDate);
			
			// exécution de la requête
			preparedStmt.execute();
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'insertion de la notation");
			e.printStackTrace();
		} 
	}

}
