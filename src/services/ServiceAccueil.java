package services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entites.Apprenant;
import entites.EnqueteSatisfaction;
import entites.ListingComplet;

public class ServiceAccueil {
	private Connection connection;

	public ServiceAccueil(Dbconfig dbconfig) {
		this.connection = dbconfig.getDbconfig();
	}

	public List<ListingComplet> selectAllFormation() {
		try {
			// Déclaration requête SQL
			Statement statement = connection.createStatement();
			// Exécution requête SQL pour sélectionner toutes les informations des formations
			ResultSet resultSet = statement.executeQuery("SELECT f.id, f.formateur, f.date_formation, f.theme, a.nom, a.prenom, a.email, e.noteQ1, e.noteQ2, e.noteQ3, e.noteQ4, (e.noteQ1+e.noteQ2+e.noteQ3+e.noteQ4) as total "
					+ "FROM formation f "
					+ "JOIN utilisateur_formation uf ON f.id = uf.id_formation "
					+ "JOIN apprenant a ON a.id = uf.id_apprenant "
					+ "JOIN enquete_satisfaction e ON e.id_formation = f.id "
					+ "AND e.id_apprenant = a.id ");

			// Création d'une liste pour stocker les résultats
			List<ListingComplet> formations = new ArrayList<>();
			// Boucle pour parcourir les résultats
			while (resultSet.next()) {
				// Récupération des informations de chaque formation
				int id = resultSet.getInt("id");
				String formateur = resultSet.getString("formateur");
				String date_formation = resultSet.getString("date_formation");
				String theme = resultSet.getString("theme");
				String nom = resultSet.getString("nom");
				String prenom = resultSet.getString("prenom");
				String email = resultSet.getString("email");
				int noteQ1 = resultSet.getInt("noteQ1");
				int noteQ2 = resultSet.getInt("noteQ2");
				int noteQ3 = resultSet.getInt("noteQ3");
				int noteQ4 = resultSet.getInt("noteQ4");
				int total = resultSet.getInt("total");
				// Ajout des informations de chaque formation à la liste
				formations.add(new ListingComplet(id, formateur, date_formation, theme, nom, prenom, email, noteQ1, noteQ2, noteQ3, noteQ4, total));
			}
			// Retourne la liste
			return formations;
		}
		catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
			// Retourne null en cas d'erreur
			return null;
		}
	}

	public List<Apprenant> selectAllApprenant() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM apprenant");

			List<Apprenant> apprenants = new ArrayList<>();
			// Enregistrement des résultats dans une liste
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String prenom = resultSet.getString("prenom");
				String email = resultSet.getString("email");
				apprenants.add(new Apprenant(id, nom,prenom,email));
			}
			return apprenants;
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
			return null;
		}
	}



	public List<EnqueteSatisfaction> selectAllEnqueteSatisfaction() {
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM enquete_satisfaction");

			List<EnqueteSatisfaction> enqueteSatisfactions = new ArrayList<>();
			// Enregistrement des résultats dans une liste
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				int id_formation = resultSet.getInt("id_formation");
				int id_apprenant = resultSet.getInt("id_apprenant");
				int noteQ1 = resultSet.getInt("noteQ1");
				int noteQ2 = resultSet.getInt("noteQ2");
				int noteQ3 = resultSet.getInt("noteQ3");
				int noteQ4 = resultSet.getInt("noteQ4");
				String date_envoi = resultSet.getString("date_envoi");
				enqueteSatisfactions.add(new EnqueteSatisfaction(id, id_formation,id_apprenant,noteQ1,noteQ2,noteQ3,noteQ4,date_envoi));
			}
			return enqueteSatisfactions;
		} catch (SQLException e) {
			System.out.println("Erreur lors de l'exécution de la requête SQL");
			e.printStackTrace();
			return null;
		}
	}
}
